//___FILEHEADER___

import Foundation
import RxSwift
import RxCocoa

class ___FILEBASENAMEASIDENTIFIER___: ViewModelType {
    
    struct Input {

    }
    
    struct Output {

    }
    
    // MARK: Subjects / Relays
    /// Input

    /// Output
    
    // MARK: Other Properties
    /// Private Properties
    
    /// Public Properties
    
    // MARK: Dependencies
    var input: Input
    var output: Output
    var disposeBag: DisposeBag
    
    // MARK: Init
    init() {
        self.input = Input()
        
        self.output = Output()
        
        self.disposeBag = DisposeBag()
    }
    
    deinit {
        debugPrint("deinit \(String(describing: type(of: self)))")
    }
}

// MARK: Network Functions
extension ___FILEBASENAMEASIDENTIFIER___ {
    
  
}

// MARK: RxFunctions
extension ___FILEBASENAMEASIDENTIFIER___ {

    /// Observable inputs should go here
    fileprivate func observeActions() {
        
    }
    
    /// Observable properties should go here
    fileprivate func observeValues() {
        
    }
}

// MARK: Logical Functions
extension ___FILEBASENAMEASIDENTIFIER___ {
    
}

