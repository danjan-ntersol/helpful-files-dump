//___FILEHEADER___

import UIKit
import SnapKit
import RxSwift
import RxCocoa

class ___FILEBASENAMEASIDENTIFIER___: BaseViewController {
    
    // MARK: UI Components
    
    // MARK: Dependencies
    
    // MARK: Init
    
    
    deinit {
        debugPrint("deinit \(String(describing: type(of: self)))")
    }
}

// MARK: Lifecycle
extension ___FILEBASENAMEASIDENTIFIER___ {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
}

// MARK: UI Functions
extension ___FILEBASENAMEASIDENTIFIER___ {
    
    /// set ui properties here
    fileprivate func prepareUi() {
        
    }
}

// MARK: RxFunctions
extension ___FILEBASENAMEASIDENTIFIER___ {

    /// observe actions not connected to ViewModel
    fileprivate func observeActions() {
        
    }
    
    /// bind/observe inputs for ViewModel
    fileprivate func observeInputs() {
        
    }
    
    /// bind/observe outputs from ViewModel
    fileprivate func observeOutputs() {
        
    }
}

// MARK: Navigation
extension ___FILEBASENAMEASIDENTIFIER___ {
    
}

// MARK: Other Functions
extension ___FILEBASENAMEASIDENTIFIER___ {
    
}

